package solutions.mlj.srovnejsito.security.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import solutions.mlj.srovnejsito.security.LoggingStrictHttpFirewall;

import java.util.Collections;
import java.util.List;

@Configuration
public class SecurityConfigurer extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	private static final String[] AUTH_WHITELIST = {
		// -- swagger ui
		"/swagger-resources", "/swagger-resources/**", "/swagger-ui", "/swagger-ui/**", "/signup", "/webjars/**", "/v3/api-docs/**", "/v3/api-docs.yaml",
		// other public endpoints of your API may be appended to this array
		"/api/public/**", "/api/form/**", "/f/**", "/favicon.ico", "/**/*.png", "/**/*.gif", "/**/*.svg", "/**/*.jpg", "/**/*.html", "/**/*.css", "/**/*.js"};

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
			.antMatchers(AUTH_WHITELIST)
			.antMatchers(HttpMethod.OPTIONS, "/**");

	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(NoOpPasswordEncoder.getInstance());
	}

	@Bean
	public JwtTokenFilter jwtAuthorizationFilter() throws Exception {
		return new JwtTokenFilter(authenticationManager());
	}

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().cors().disable()
	        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)     //no cookies
	        .and()
	        .authorizeRequests()
	        .antMatchers("/login").permitAll()
	        .antMatchers("/api/account/login").permitAll()
                .antMatchers("/api/public/**").permitAll()
                .antMatchers("/swagger-ui/**").permitAll()
                .antMatchers("/api/account/single-sign-on").permitAll()
	        .anyRequest().authenticated()
		    .and()
		    .addFilter(jwtAuthorizationFilter());
	}


	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		List<String> permitAll = Collections.singletonList(CorsConfiguration.ALL);

		List<String> allowedOrigins = true ? permitAll : null;
		//configuration.setAllowedOrigins(allowedOrigins);
		configuration.setAllowedOriginPatterns(allowedOrigins);
		configuration.setAllowedMethods(permitAll);
		configuration.setAllowedHeaders(permitAll);
		configuration.setAllowCredentials(true);
		configuration.setMaxAge(3600L);

		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}


}
