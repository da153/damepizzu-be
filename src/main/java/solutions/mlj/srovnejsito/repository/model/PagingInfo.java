package solutions.mlj.srovnejsito.repository.model;

import lombok.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import solutions.mlj.srovnejsito.model.SortType;

import java.util.List;
import java.util.stream.Collectors;

@Value
public class PagingInfo {

	int pageNumber;

	int pageSize;

	SortType sortType;

	public Pageable getPageable() {
		return PageRequest.of(pageNumber, pageSize, getSort());
	}

	public Sort getSort() {
		return Sort.by(sortType.getSortOrders());
	}

	public PageRequest getPageableWithPrepend(String prepend) {
		List<Sort.Order> prependedSortOrders = sortType.getSortOrders()
			.stream()
			.map(sortOrder -> new Sort.Order(sortOrder.getDirection(), prepend + sortOrder.getProperty()))
			.collect(Collectors.toList());

		Sort sort = Sort.by(prependedSortOrders);
		return PageRequest.of(pageNumber, pageSize, sort);
	}

}
