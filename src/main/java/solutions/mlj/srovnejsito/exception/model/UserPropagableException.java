package solutions.mlj.srovnejsito.exception.model;

public class UserPropagableException extends IllegalArgumentException {

	public UserPropagableException() {
	}

	public UserPropagableException(String s) {
		super(s);
	}
}
