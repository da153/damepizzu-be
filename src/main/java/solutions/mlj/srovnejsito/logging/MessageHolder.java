package solutions.mlj.srovnejsito.logging;


import solutions.mlj.srovnejsito.logging.model.Message;

public class MessageHolder {

	private static final ThreadLocal<Message> messageHolder = new ThreadLocal<>();

	public static void set(Message message) {
		messageHolder.set(message);
	}

	public static Message get() {
		return messageHolder.get();
	}

	public static void remove() {
		messageHolder.remove();
	}

}
