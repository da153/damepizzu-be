package solutions.mlj.srovnejsito.security;

import org.springframework.security.web.firewall.FirewalledRequest;
import org.springframework.security.web.firewall.RequestRejectedException;
import org.springframework.security.web.firewall.StrictHttpFirewall;

import javax.servlet.http.HttpServletRequest;

public class LoggingStrictHttpFirewall extends StrictHttpFirewall {

    @Override
    public FirewalledRequest getFirewalledRequest(HttpServletRequest request) throws RequestRejectedException {
        try {
            return super.getFirewalledRequest(request);
        } catch (RequestRejectedException e) {
            String fullUrl = request.getRequestURL().toString() + (request.getQueryString() == null ? "" : "?" + request.getQueryString());
            throw new IllegalArgumentException("Requested uri that threw exception: " + fullUrl, e);
        }
    }
}
