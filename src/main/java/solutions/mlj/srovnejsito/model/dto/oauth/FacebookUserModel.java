package solutions.mlj.srovnejsito.model.dto.oauth;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class FacebookUserModel {
	private String id;
	@JsonProperty("first_name")
	@SerializedName("first_name")
	private String firstName;
	@JsonProperty("last_name")
	@SerializedName("last_name")
	private String lastName;
	private String name;
	private String email;
}
