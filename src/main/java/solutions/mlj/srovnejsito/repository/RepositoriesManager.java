package solutions.mlj.srovnejsito.repository;

import java.util.Optional;

public interface RepositoriesManager {

	<T> Optional<BaseRepository> getRepositoryOfType(Class<T> type);

}
