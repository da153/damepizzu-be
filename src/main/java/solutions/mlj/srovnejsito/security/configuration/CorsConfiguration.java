package solutions.mlj.srovnejsito.security.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfiguration implements WebMvcConfigurer {

    //@Value("${security.cors.enable}")
    //private boolean enableCors;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        if (false) {
            registry.addMapping("/**")
                    .allowedMethods("*")
                    .allowCredentials(true)
                    .allowedOriginPatterns("*")
                    .maxAge(3600L);
        }
    }

}
