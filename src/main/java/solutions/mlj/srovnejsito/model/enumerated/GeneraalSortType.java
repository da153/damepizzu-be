package solutions.mlj.srovnejsito.model.enumerated;

import org.springframework.data.domain.Sort;
import solutions.mlj.srovnejsito.model.SortType;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public enum GeneraalSortType implements SortType {
	ID_ASC, ID_DESC, SORT_ASC, SORT_DESC;

	@Override
	public List<Sort.Order> getSortOrders() {
		switch (this) {
			case SORT_ASC:
			case SORT_DESC:
				return getSortOrders("sort");
			case ID_ASC:
			case ID_DESC:
				return getSortOrders("id");
		}
		throw new IllegalStateException("Cannot find sort order for " + this.name());
	}

	@Override
	public Sort.Direction getDirection() {
		return this.name()
			.toLowerCase()
			.endsWith("desc") ? Sort.Direction.DESC : Sort.Direction.ASC;
	}

	private List<Sort.Order> getSortOrders(String... properties) {
		return Arrays.stream(properties)
			.map(property -> new Sort.Order(getDirection(), property))
			.collect(Collectors.toList());
	}
}
