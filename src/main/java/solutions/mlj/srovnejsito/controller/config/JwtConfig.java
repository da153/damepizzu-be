package solutions.mlj.srovnejsito.controller.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Validated
@Configuration()
@Data
@ConfigurationProperties(prefix = "jwt")
public class JwtConfig {

	@NotEmpty
	private String secret;

	@NotNull
	private int expiration;
}
