package solutions.mlj.srovnejsito.validator.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import solutions.mlj.srovnejsito.exception.model.UserPropagableException;
import solutions.mlj.srovnejsito.model.domain.Account;
import solutions.mlj.srovnejsito.model.enumerated.Role;
import solutions.mlj.srovnejsito.repository.AccountRepository;
import solutions.mlj.srovnejsito.security.model.AuthenticatedUser;
import solutions.mlj.srovnejsito.validator.Validator;

@Component
@AllArgsConstructor
public class AccountValidator implements Validator<Account> {

    private final AccountRepository accountRepository;

    @Override
    public void validateBeforeCreation(Account account) {
        validateUsernameNotExists(account);
    }

    @Override
    public void validateBeforeEditing(Account account) {
        validateUsernameNotExists(account);
        validateManagerDoesNotChangeHisRole(account);
    }

    private void validateUsernameNotExists(Account account) {
        Account existingAccount = accountRepository.getByUsername(account.getUsername());
        boolean alreadyExists = existingAccount != null && !existingAccount.getId().equals(account.getId());
        if (alreadyExists) {
            throw new UserPropagableException(String.format("Account with username %s already exists.", account.getUsername()));
        }
    }

    private void validateManagerDoesNotChangeHisRole(Account account) {
        AuthenticatedUser currentUser= AuthenticatedUser.getCurrent();
        if (account.getId() == currentUser.getAccountId()) {
            accountRepository.clearCache();
            Account currentAccount = accountRepository.getNotNull(account.getId());
            boolean managerChangedHisRole = currentAccount.getRole() == Role.MANAGER && currentAccount.getRole() != account.getRole();
            if (managerChangedHisRole) {
                throw new UserPropagableException("Currently logged-in manager cannot change his own role.");
            }
        }
    }

}
