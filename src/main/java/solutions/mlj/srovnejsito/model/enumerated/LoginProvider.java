package solutions.mlj.srovnejsito.model.enumerated;

public enum LoginProvider {
	FACEBOOK, EMAIL
}
