package solutions.mlj.srovnejsito.model.enumerated;

public enum Role {

    MANAGER,
    CASHIER;

    public static final String MANAGER_STRING = "ROLE_MANAGER";
    public static final String CASHIER_STRING = "ROLE_CASHIER";

    public String getRoleString() {
        switch (this) {
            case MANAGER:
                return MANAGER_STRING;
            case CASHIER:
                return CASHIER_STRING;
        }
        throw new IllegalStateException("Could not find String representation of Role " + this.name());
    }
}
