package solutions.mlj.srovnejsito.logging.interceptor;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@AllArgsConstructor
public class LoggingInterceptorConfigurer implements WebMvcConfigurer {

	private MessageLoggingInterceptor messageLoggingInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(messageLoggingInterceptor)
			.addPathPatterns("/api/**");
	}

}
