package solutions.mlj.srovnejsito.engine.domain;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import solutions.mlj.srovnejsito.model.enumerated.Role;

@Data
@RequiredArgsConstructor
public class NewAccount {

    private final String username;

    private String password;

    private final Role role;

}
