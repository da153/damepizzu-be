package solutions.mlj.srovnejsito.security;


import solutions.mlj.srovnejsito.security.model.AuthenticatedUser;

public interface AuthenticatedUserService {

    public AuthenticatedUser getUser(String username);

    public void updateLastLoginTime(int accountId);

}
