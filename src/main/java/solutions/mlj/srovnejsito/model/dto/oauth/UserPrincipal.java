package solutions.mlj.srovnejsito.model.dto.oauth;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import solutions.mlj.srovnejsito.model.domain.Account;

import java.util.Collection;
import java.util.List;

public class UserPrincipal implements UserDetails {
	private Account account;
	public UserPrincipal() {
	}
	public UserPrincipal(Account user) {
		super();
		account = user;
	}
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return account.getAuthorities();
	}

	@Override
	public String getPassword() {
		return account.getPassword();
	}
	@Override
	public String getUsername() {
		return account.getEmail();
	}
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	@Override
	public boolean isEnabled() {
		return true;
	}

	public Account getAccount() {
		return account;
	}

}
