package solutions.mlj.srovnejsito.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PageDto<T> {

	@Schema(description = "Current page index, starts at 0", example = "0", required = true)
	public Integer pageNumber;

	@Schema(description = "Total number number of pages", example = "8", required = true)
	public Integer totalPages;

	@Schema(description = "If there are available more pages with more content", example = "true", required = true)
	public Boolean hasNextPage;

	@Schema(description = "Maximum number of elements per page", example = "50", required = true)
	public Integer elementsPerPage;

	@Schema(description = "Number of elements on current page", example = "32", required = true)
	public Integer numberOfElements;

	@Schema(description = "Number of elements on current page", example = "32", required = true)
	public Integer totalNumberOfElements;

	@Schema(description = "Elements on current page", required = true)
	public List<T> elements;

}
