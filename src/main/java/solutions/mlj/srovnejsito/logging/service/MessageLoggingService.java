package solutions.mlj.srovnejsito.logging.service;

public interface MessageLoggingService {

	void handleRequest(String uri, String method, String urlParams);

	void handleResponse();

	void handleException(Exception exception);

	void setRequestResponse(String request, String response);

	void fileLog();

}
