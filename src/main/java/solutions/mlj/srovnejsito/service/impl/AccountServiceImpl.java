package solutions.mlj.srovnejsito.service.impl;

import com.google.firebase.messaging.FirebaseMessagingException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import solutions.mlj.srovnejsito.engine.domain.NewAccount;
import solutions.mlj.srovnejsito.exception.model.UserPropagableException;
import solutions.mlj.srovnejsito.mapper.AccountMapper;
import solutions.mlj.srovnejsito.model.domain.Account;
import solutions.mlj.srovnejsito.model.dto.AccountDto;
import solutions.mlj.srovnejsito.model.dto.SingleSingOnDto;
import solutions.mlj.srovnejsito.model.dto.TokenDto;
import solutions.mlj.srovnejsito.model.dto.create.LoginDto;
import solutions.mlj.srovnejsito.model.dto.create.NewAccountDto;
import solutions.mlj.srovnejsito.model.dto.oauth.FacebookUserModel;
import solutions.mlj.srovnejsito.model.enumerated.LoginProvider;
import solutions.mlj.srovnejsito.model.enumerated.Role;
import solutions.mlj.srovnejsito.repository.AccountRepository;
import solutions.mlj.srovnejsito.security.SecurityUtils;
import solutions.mlj.srovnejsito.security.model.AuthenticatedUser;
import solutions.mlj.srovnejsito.service.AccountService;
import solutions.mlj.srovnejsito.service.FirebaseMessagingService;
import solutions.mlj.srovnejsito.util.JwtTokenizer;
import solutions.mlj.srovnejsito.validator.ValidationManager;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

	private final AccountRepository accountRepository;

	private final AccountMapper accountMapper;

	private final JwtTokenizer jwtTokenizer;

	private final ValidationManager validationManager;

	private final AuthService authService;

	private final PasswordEncoder passwordEncoder;

	private final FirebaseMessagingService firebaseMessagingService;

	public AccountServiceImpl(AccountRepository accountRepository,
	                          AccountMapper accountMapper,
	                          JwtTokenizer jwtTokenizer,
	                          ValidationManager validationManager,
	                          FirebaseMessagingService firebaseMessagingService,
	                          AuthService authService) {
		this.accountRepository = accountRepository;
		this.accountMapper = accountMapper;
		this.jwtTokenizer = jwtTokenizer;
		this.validationManager = validationManager;
		this.firebaseMessagingService = firebaseMessagingService;
		this.authService = authService;
		this.passwordEncoder = new BCryptPasswordEncoder();
	}

	@Override
	public void resetPassword(String username) {
		NewAccount newAccount = new NewAccount(username, null);
		//Account account = accountManager.changePassword(newAccount);
	}

	@Override
	public void changePassword(String password) {
		if (password.length() < 6) {
			throw new IllegalArgumentException("Password requires at least 6 characters.");
		}
        /*
        AuthenticatedUser currentUser = AuthenticatedUser.getCurrent();

        String username = accountManager.getAccount(currentUser.getAccountId()).getUsername();

        NewAccount newAccount = new NewAccount(username, null);
        newAccount.setPassword(password);
        accountManager.changePassword(newAccount);

         */
	}

	@Override
	public AccountDto getCurrentAccount() {
		AuthenticatedUser currentUser = AuthenticatedUser.getCurrent();
		Account account = accountRepository.getNotNull(currentUser.getAccountId());
		return accountMapper.accountToAccountDto(account);
	}

	@Override
	public AccountDto createAccount(NewAccountDto newAccountDto) {
		final Account account = accountMapper.newAccountDtoToAccount(newAccountDto);
		String encryptedPassword = SecurityUtils.encryptPassword("admin"); // TODO ADMUN PASSWORD
		account.setPassword(encryptedPassword);
		validationManager.validateBeforeCreation(account);
		Account createdAccount = accountRepository.save(account);
		return accountMapper.accountToAccountDto(createdAccount);
	}


	@Override
	public AccountDto editAccount(NewAccountDto newAccountDto, int accountId) {
		Account editBy = accountMapper.newAccountDtoToAccount(newAccountDto);
		Account accountToEdit = accountRepository.getNotNull(accountId);

		accountMapper.edit(accountToEdit, editBy);
		validationManager.validateBeforeEditing(accountToEdit);

		Account editedAccount = accountRepository.save(accountToEdit);
		return accountMapper.accountToAccountDto(editedAccount);
	}

	@Override
	public List<AccountDto> getAllAccounts() {
		List<Account> accounts = accountRepository.getAllAccounts();
		return accountMapper.accountsToAccountDtos(accounts);
	}

	@Override
	public void deleteAccount(int accountId) {
		//accountManager.deleteAccount(accountId);
	}

	@Override
	public ResponseEntity<TokenDto> singleSignOn(SingleSingOnDto request) {
		FacebookUserModel facebookUserModel = authService.facebook(request.getToken());
		final Optional<Account> userOptional = Optional.ofNullable(accountRepository.getByEmail(facebookUserModel.getEmail()));
		if (userOptional.isEmpty()) {
			Account account = new Account();
			account.setProvider(LoginProvider.FACEBOOK);
			account.setEmail(facebookUserModel.getEmail());
			account.setFirstName(facebookUserModel.getFirstName());
			account.setLastName(facebookUserModel.getLastName());
			account.setRole(Role.CASHIER);
			account.setUsername(facebookUserModel.getName());
			account.setCreated(LocalDateTime.now());

			final Account saved = accountRepository.save(account);
			String token = jwtTokenizer.generateTokenForAccount(saved);
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.set(HttpHeaders.AUTHORIZATION, token);
			return ResponseEntity.ok()
				.headers(responseHeaders)
				.body(new TokenDto(token));
		} else { // user exists just login
			final Account user = userOptional.get();
			if ((user.getProvider() != LoginProvider.FACEBOOK)) { //check if logged in with different logged in method
				return ResponseEntity.badRequest()
					.build();
			}
			String token = jwtTokenizer.generateTokenForAccount(user);
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.set(HttpHeaders.AUTHORIZATION, token);
			return ResponseEntity.ok()
				.headers(responseHeaders)
				.body(new TokenDto(token));
		}
	}

	@Override
	public TokenDto login(LoginDto loginDto) {
		final Account account = accountRepository.getByEmail(loginDto.getEmail());
		if (account != null) {
			if (passwordEncoder.matches(loginDto.getPassword(), account.getPassword())) {
				var token = jwtTokenizer.generateTokenForAccount(account);
				return new TokenDto(token);
			}
			throw new UserPropagableException("Bad username or password");
		}
		throw new UserPropagableException("Bad username or password");
	}

	@Override
	public void sendNotification(String title, String message) {
		final String firebaseToken = getCurrentAccount().getFirebaseToken();

		try {
			firebaseMessagingService.sendNotification(title, message, firebaseToken);
		} catch (FirebaseMessagingException e) {
			throw new UserPropagableException("Could not send token");
		}
	}

	@Override
	public HttpEntity<AccountDto> findAccount() {
		return ResponseEntity.ok()
			.body(getCurrentAccount());
	}

	@Override
	public void subscribeToNotifications(String token) {
		final Account account = accountRepository.getByEmail(getCurrentAccount().getEmail());
		account.setFirebaseToken(token);
		accountRepository.save(account);
	}
}
