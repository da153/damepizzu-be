package solutions.mlj.srovnejsito.service.impl;

import io.minio.ObjectStat;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import solutions.mlj.srovnejsito.mapper.FileMapper;
import solutions.mlj.srovnejsito.mapper.PageMetaInfoMapper;
import solutions.mlj.srovnejsito.model.domain.File;
import solutions.mlj.srovnejsito.model.dto.PageDto;
import solutions.mlj.srovnejsito.model.dto.file.FileDto;
import solutions.mlj.srovnejsito.model.enumerated.GeneraalSortType;
import solutions.mlj.srovnejsito.repository.FileStorage;
import solutions.mlj.srovnejsito.repository.impl.ImageDbRepository;
import solutions.mlj.srovnejsito.repository.model.PagingInfo;
import solutions.mlj.srovnejsito.service.FileService;
import solutions.mlj.srovnejsito.util.Tuple;

import java.util.List;
import java.util.UUID;

@Service
@Slf4j
@AllArgsConstructor
public class FileServiceImpl implements FileService {

	private final FileStorage fileStorage;

	private final ImageDbRepository imageDbRepository;

	private final FileMapper fileMapper;

	@Override
	public FileDto getFile(UUID id) {
		File file = imageDbRepository.getNotNull(id);
		return fileMapper.entityToDto(file);
	}

	@Override
	public FileDto uploadFile(MultipartFile f) {
		ObjectStat metadata = fileStorage.save(f.getName(), f);
		File file = fileMapper.fileAndMetadataToDbEntity(f, metadata);
		imageDbRepository.save(file);
		return fileMapper.entityToDto(file);
	}

	@Override
	public void deleteFile(UUID id) {
		File file = imageDbRepository.getNotNull(id);
		fileStorage.delete(file.getName());
		imageDbRepository.deleteById(id);
	}

	@Override
	public PageDto<FileDto> getAllFiles(Integer pageNumber,
	                                    Integer pageSize,
	                                    String searchQuery,
	                                    GeneraalSortType sortType) {
		if (pageSize <= 0) {
			pageSize = 1;
		}

		PagingInfo pagingInfo = new PagingInfo(pageNumber, pageSize, sortType);
		Page<File> allFiles = imageDbRepository.findAllFiles(searchQuery, pagingInfo);
		PageDto<FileDto> pageDto = PageMetaInfoMapper.mapPageMetaInfo(allFiles);
		List<FileDto> allFilesDtos = fileMapper.entitiesToDto(allFiles.getContent());
		pageDto.setElements(allFilesDtos);
		return pageDto;
	}

	@Override
	public Tuple<byte[], ObjectStat> serveFile(UUID id) {
		File foundedFile = imageDbRepository.getNotNull(id);
		return fileStorage.getWithMetadata(foundedFile.getName());
	}

}
