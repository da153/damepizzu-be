package solutions.mlj.srovnejsito.logging.interceptor;


import lombok.AllArgsConstructor;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import org.springframework.web.util.WebUtils;
import solutions.mlj.srovnejsito.logging.MessageHolder;
import solutions.mlj.srovnejsito.logging.service.MessageLoggingService;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//@Component
@AllArgsConstructor
public class MessageLoggingFilter implements Filter {

	private MessageLoggingService messageLoggingService;

	@Override
	public void doFilter(ServletRequest servletRequest,
	                     ServletResponse servletResponse,
	                     FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest requestToCache = new ContentCachingRequestWrapper((HttpServletRequest) servletRequest);
		HttpServletResponse responseToCache = new ContentCachingResponseWrapper((HttpServletResponse) servletResponse);

		try {
			filterChain.doFilter(requestToCache, responseToCache);
		} finally {
			ContentCachingRequestWrapper requestWrapper = WebUtils.getNativeRequest(requestToCache,
				ContentCachingRequestWrapper.class
			);
			byte[] bytes = requestWrapper.getContentAsByteArray();
			String requestBody = new String(bytes, requestWrapper.getCharacterEncoding());
			requestBody = requestBody.isEmpty() ? null : requestBody;

			ContentCachingResponseWrapper responseWrapper = WebUtils.getNativeResponse(responseToCache,
				ContentCachingResponseWrapper.class
			);
			byte[] responseBytes = responseWrapper.getContentAsByteArray();
			String responseBody = new String(responseBytes, responseWrapper.getCharacterEncoding());
			responseBody = responseBody.isEmpty() ? null : responseBody;
			responseWrapper.copyBodyToResponse();

			messageLoggingService.setRequestResponse(requestBody, responseBody);
			messageLoggingService.fileLog();
			MessageHolder.remove();
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void destroy() {

	}

}
