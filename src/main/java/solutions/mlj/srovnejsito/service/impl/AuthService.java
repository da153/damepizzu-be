package solutions.mlj.srovnejsito.service.impl;

import lombok.AllArgsConstructor;
import org.apache.http.util.TextUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import solutions.mlj.srovnejsito.controller.config.oauth.FacebookClient;
import solutions.mlj.srovnejsito.exception.model.UserPropagableException;
import solutions.mlj.srovnejsito.model.dto.oauth.FacebookAuthModel;
import solutions.mlj.srovnejsito.model.dto.oauth.FacebookUserModel;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
public class AuthService {

	private final FacebookClient facebookClient;

	public FacebookUserModel facebook(String token) {
		Call<FacebookUserModel> call = facebookClient.getFaceBookUser(String.join(",", Arrays.asList("email","first_name","last_name","name")), token);
		try {
			Response<FacebookUserModel> response = call.execute();
			if (response.isSuccessful()) {
				return response.body();
			}
			throw new UserPropagableException("Cannot get user by this token!");
		} catch (Exception e) {
			throw new UserPropagableException("Cannot get user by this token!");
		}
	}
}
