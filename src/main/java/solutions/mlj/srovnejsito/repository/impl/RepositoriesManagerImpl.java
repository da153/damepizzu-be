package solutions.mlj.srovnejsito.repository.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Component;
import solutions.mlj.srovnejsito.repository.BaseRepository;
import solutions.mlj.srovnejsito.repository.RepositoriesManager;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Slf4j
@SuppressWarnings("rawtypes")
public class RepositoriesManagerImpl implements RepositoriesManager {

	private final List<BaseRepository> validators;

	public <T> Optional<BaseRepository> getRepositoryOfType(Class<T> type) {
		Optional<BaseRepository> validator = validators.stream()
			.filter(v -> {
				Class<?>[] classes = GenericTypeResolver.resolveTypeArguments(v.getClass(), BaseRepository.class);
				return classes[0] == type;
			})
			.findAny();
		if (!validator.isPresent()) {
			log.error("Cannot find validator for type " + type.getName() + ", validation process will be skipped.");
		}
		return validator;
	}

}
