package solutions.mlj.srovnejsito.mapper.decorator;

import org.springframework.beans.factory.annotation.Autowired;
import solutions.mlj.srovnejsito.mapper.AccountMapper;
import solutions.mlj.srovnejsito.model.domain.Account;

public abstract class AccountMapperDecorator extends EditingMapperImpl<Account> implements AccountMapper {

    private AccountMapper delegate;

    @Autowired
    public void setDelegate(AccountMapper delegate) {
        this.delegate = delegate;
    }

}
