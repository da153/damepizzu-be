DROP TABLE IF EXISTS "account"."account";

CREATE TABLE "account"."account" (
    "id" serial primary key,
    "username" varchar(60) DEFAULT NULL,
    "password" char(60) DEFAULT NULL,
    "role" varchar(20)  NOT NULL,
    "created" timestamptz default current_timestamp,
    "last_login" timestamptz default current_timestamp,
    "first_name" varchar(45)  NOT NULL,
    "last_name" varchar(45)  NOT NULL,
    "email" varchar(45)  NOT NULL,
    "phone" varchar(45) DEFAULT NULL,
    "provider" varchar(45) DEFAULT 'EMAIL'
    )
