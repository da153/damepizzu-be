package solutions.mlj.srovnejsito.logging.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Getter
@Setter
@NoArgsConstructor
public class Message {

	private final static String STRING_DELIMITER = "; ";

	private LocalDateTime requestTime;

	private LocalDateTime responseTime;

	private String user;

	private Double time;

	private String method;

	private String uri;

	private String urlParams;

	private String requestBody;

	private String responseBody;

	private String exception;

	private String exceptionMessage;

	private double calculateTime() {
		if (requestTime == null || responseTime == null) {
			return 0;
		}
		return requestTime.until(responseTime, ChronoUnit.MILLIS);
	}

	private String resolveFullUri() {
		return StringUtils.hasText(urlParams) ? uri + "?" + urlParams : uri;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Request time: ")
			.append(requestTime)
			.append(STRING_DELIMITER)
			.append("Response time: ")
			.append(responseTime)
			.append(STRING_DELIMITER)
			.append("Process time: ")
			.append(calculateTime())
			.append("ms")
			.append(STRING_DELIMITER)
			.append("User: ")
			.append(user)
			.append(STRING_DELIMITER)
			.append("URI: ")
			.append(resolveFullUri())
			.append(STRING_DELIMITER)
			.append("Method: ")
			.append(method)
			.append(STRING_DELIMITER)
			.append("Request body: ")
			.append(requestBody)
			.append(STRING_DELIMITER)
			.append("Response body: ")
			.append(responseBody);

		if (StringUtils.hasText(exception)) {
			stringBuilder.append(STRING_DELIMITER)
				.append("Exception: ")
				.append(exception)
				.append(STRING_DELIMITER)
				.append("Message: ")
				.append(exceptionMessage);
		}
		return stringBuilder.toString();
	}

}
