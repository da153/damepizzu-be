package solutions.mlj.srovnejsito.model.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import solutions.mlj.srovnejsito.model.enumerated.LoginProvider;
import solutions.mlj.srovnejsito.model.enumerated.Role;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String username;

    private String password;

    @Enumerated(EnumType.STRING)
    private Role role;

    private LocalDateTime created;

    private LocalDateTime lastLogin;

    private String firstName;

    private String lastName;

    private String email;

    private String phone;

	private String firebaseToken;

	@Enumerated(EnumType.STRING)
	private LoginProvider provider = LoginProvider.EMAIL;

	public List<SimpleGrantedAuthority> getAuthorities() {
		return List.of(new SimpleGrantedAuthority(this.getRole().getRoleString()));
	}

}
