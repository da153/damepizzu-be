package solutions.mlj.srovnejsito.model.dto.create;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;
import solutions.mlj.srovnejsito.model.enumerated.Role;

import javax.validation.constraints.*;

@Data
@NoArgsConstructor
public class NewAccountDto {

    @Schema(description = "Username of the User", example = "Jan", required = true)
    @NotBlank(message = "Username must be provided.")
    @Size(min = 1, max = 45, message = "Username must have max 45 characters.")
    private String username;

    @Schema(description = "Role of the User", example = "MANAGER", required = true)
    @NotNull(message = "User role must be provided.")
    private Role role;

    @Schema(description = "First name of the User", example = "Jan", required = true)
    @NotBlank(message = "User first name must be provided.")
    @Size(min = 1, max = 45, message = "User first name must have max 45 characters.")
    private String firstName;

    @Schema(description = "Last name of the User", example = "Novák", required = true)
    @NotBlank(message = "User last name must be provided.")
    @Size(min = 1, max = 45, message = "User last name must have max 45 characters.")
    private String lastName;

    @Schema(description = "Email of the User", example = "jan.novak@seznam.cz", required = true)
    @NotBlank(message = "User email must be provided.")
    @Size(min = 1, max = 45, message = "User email must have max 45 characters.")
    @Email(message = "User email must be a valid email address.")
    private String email;

    @Schema(description = "Phone of the User", example = "777568951")
    @Size(max = 20, message = "User phone must have max 20 characters.")
    @Pattern(message = "User phone must be a valid phone number.", regexp = "^\\+?[0-9]*$")
    private String phone;

}
