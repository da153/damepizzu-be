package solutions.mlj.srovnejsito.model.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import solutions.mlj.srovnejsito.exception.model.UserPropagableException;

@Data
@NoArgsConstructor
public class ExceptionDto {

	private String message;

	private Boolean propagateToUser = false;

	public ExceptionDto(String message) {
		this.message = message;
	}

	public static ExceptionDto fromException(Exception exception) {
		ExceptionDto exceptionDto = new ExceptionDto(exception.getMessage());
		if (exception instanceof UserPropagableException) {
			exceptionDto.setPropagateToUser(true);
		}
		return exceptionDto;
	}

}
