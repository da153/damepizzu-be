package solutions.mlj.srovnejsito.repository;

import solutions.mlj.srovnejsito.model.domain.Account;

import java.util.List;
import java.util.Optional;

public interface AccountRepository extends BaseRepository<Account, Integer> {

    public Account getByUsername(String username);

    public Account getByEmail(String email);

    public List<Account> getAllAccounts();

}
