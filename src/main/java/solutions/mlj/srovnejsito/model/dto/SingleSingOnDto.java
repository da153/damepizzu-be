package solutions.mlj.srovnejsito.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import solutions.mlj.srovnejsito.model.enumerated.LoginProvider;

@Data
@NoArgsConstructor
public class SingleSingOnDto {
	public LoginProvider provider;
	public String token;
	public String email;
}
