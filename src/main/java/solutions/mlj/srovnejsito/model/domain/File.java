package solutions.mlj.srovnejsito.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import solutions.mlj.srovnejsito.model.converter.FileTypeConverter;
import solutions.mlj.srovnejsito.model.enumerated.FileType;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class File {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", updatable = false, nullable = false)
	private UUID id;

	private String name;

	private Long size;

	@NotNull
	@Convert(converter = FileTypeConverter.class)
	private FileType type;

	private String eTag;

	private Integer hash;

	public void getFileType(String type) {
		if (type.contains("image/")) {
			this.setType(FileType.IMAGE);
		} else {
			this.setType(FileType.FILE);
		}
	}

}
