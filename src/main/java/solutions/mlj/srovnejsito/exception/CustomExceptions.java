package solutions.mlj.srovnejsito.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import solutions.mlj.srovnejsito.controller.config.ExceptionPropertiesConfig;

import javax.persistence.EntityNotFoundException;
import java.text.MessageFormat;
import java.util.Optional;

@Component
public class CustomExceptions {

	private static ExceptionPropertiesConfig propertiesConfig;

	@Autowired
	public CustomExceptions(ExceptionPropertiesConfig propertiesConfig) {
		CustomExceptions.propertiesConfig = propertiesConfig;
	}

	public static RuntimeException throwException(String messageTemplate, String... args) {
		return new RuntimeException(format(messageTemplate, args));
	}

	public static RuntimeException throwException(EntityType entityType, ExceptionType exceptionType, String... args) {
		String messageTemplate = getMessageTemplate(entityType, exceptionType);
		return throwException(exceptionType, messageTemplate, args);
	}

	public static RuntimeException throwExceptionWithId(EntityType entityType,
	                                                    ExceptionType exceptionType,
	                                                    String id,
	                                                    String... args) {
		String messageTemplate = getMessageTemplate(entityType, exceptionType).concat(".")
			.concat(id);
		return throwException(exceptionType, messageTemplate, args);
	}

	public static RuntimeException throwExceptionWithTemplate(EntityType entityType,
	                                                          ExceptionType exceptionType,
	                                                          String messageTemplate,
	                                                          String... args) {
		return throwException(exceptionType, messageTemplate, args);
	}

	private static String getMessageTemplate(EntityType entityType, ExceptionType exceptionType) {
		return entityType.name()
			.concat(".")
			.concat(exceptionType.getValue())
			.toLowerCase();
	}

	private static RuntimeException throwException(ExceptionType exceptionType,
	                                               String messageTemplate,
	                                               String... args) {
		if (ExceptionType.ENTITY_NOT_FOUND.equals(exceptionType)) {
			return new EntityNotFoundException(format(messageTemplate, args));
		}
		return new RuntimeException(format(messageTemplate, args));
	}


	private static String format(String template, String... args) {
		Optional<String> templateContent = Optional.ofNullable(propertiesConfig.getConfigValue(template));
		return templateContent.map(s -> MessageFormat.format(s, args))
			.orElseGet(() -> String.format(template, args));
	}

}
