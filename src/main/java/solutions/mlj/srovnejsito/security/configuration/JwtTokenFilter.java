package solutions.mlj.srovnejsito.security.configuration;

import lombok.AllArgsConstructor;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import solutions.mlj.srovnejsito.model.domain.Account;
import solutions.mlj.srovnejsito.model.dto.oauth.UserPrincipal;
import solutions.mlj.srovnejsito.repository.AccountRepository;
import solutions.mlj.srovnejsito.util.JwtTokenizer;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;

class JwtTokenFilter extends BasicAuthenticationFilter {

	@Autowired
	private JwtTokenizer jwtUtil;

	@Autowired
	private AccountRepository accountRepository;

	public JwtTokenFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
		String header = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (header == null || !header.startsWith("Bearer")) {
			chain.doFilter(request, response);
			return;
		}

		Authentication authentication = getUsernamePasswordAuthentication(request);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(request, response);
	}

	private Authentication getUsernamePasswordAuthentication(HttpServletRequest request) {
		String token = request.getHeader(HttpHeaders.AUTHORIZATION)
			.replace("Bearer ", "");

		if (StringUtils.hasText(token) && jwtUtil.validateToken(token)) {
			String email = jwtUtil.getUsername(token);

			if (email != null) {
				Optional<Account> user = Optional.ofNullable(accountRepository.getByEmail(email));
				if (user.isPresent()) {
					UserPrincipal principal = new UserPrincipal(user.get());
					Collection<? extends GrantedAuthority> authorities = principal.getAuthorities();
					return new UsernamePasswordAuthenticationToken(
						principal,
						null,
						principal.getAuthorities()
					);
				}
			}
			return null;
		}
		return null;
	}
}
