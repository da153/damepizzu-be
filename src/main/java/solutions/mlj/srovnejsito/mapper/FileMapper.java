package solutions.mlj.srovnejsito.mapper;

import io.minio.ObjectStat;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.web.multipart.MultipartFile;
import solutions.mlj.srovnejsito.mapper.decorator.FileMapperDecorator;
import solutions.mlj.srovnejsito.model.domain.File;
import solutions.mlj.srovnejsito.model.dto.file.FileDto;

import java.util.List;

@Mapper(componentModel = "spring")
@DecoratedWith(FileMapperDecorator.class)
public interface FileMapper extends EditingMapper<File> {

	@Mapping(target = "url", ignore = true)
	FileDto entityToDto(File file);

	List<FileDto> entitiesToDto(List<File> files);

	File fileAndMetadataToDbEntity(MultipartFile file, ObjectStat metadata);

}
