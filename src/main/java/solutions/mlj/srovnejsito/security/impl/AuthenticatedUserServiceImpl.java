package solutions.mlj.srovnejsito.security.impl;

import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import solutions.mlj.srovnejsito.model.domain.Account;
import solutions.mlj.srovnejsito.repository.AccountRepository;
import solutions.mlj.srovnejsito.security.AuthenticatedUserService;
import solutions.mlj.srovnejsito.security.model.AuthenticatedUser;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Set;

@Service
@AllArgsConstructor
public class AuthenticatedUserServiceImpl implements AuthenticatedUserService {

    private AccountRepository accountRepository;

    @Override
    public AuthenticatedUser getUser(String username) {
        Account account = accountRepository.getByUsername(username);
        if (account == null) {
            throw new UsernameNotFoundException(String.format("Username %s not found!", username));
        }
        GrantedAuthority authority = new SimpleGrantedAuthority(account.getRole().getRoleString());
        Set<GrantedAuthority> authorities = Collections.singleton(authority);
        return new AuthenticatedUser(account.getId(), account.getEmail());
    }

    @Override
    public void updateLastLoginTime(int accountId) {
        Account account = accountRepository.getById(accountId);
        Assert.notNull(account, "Cannot find account with id: " + accountId);
        account.setLastLogin(LocalDateTime.now());
        accountRepository.save(account);
    }

}
