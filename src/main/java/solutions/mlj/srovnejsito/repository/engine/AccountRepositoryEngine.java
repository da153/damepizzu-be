package solutions.mlj.srovnejsito.repository.engine;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import solutions.mlj.srovnejsito.model.domain.Account;

import java.util.List;
import java.util.Optional;

public interface AccountRepositoryEngine extends CrudRepository<Account, Integer> {

	Account findByUsername(String username);

	Account findByEmail(String email);

	Account findById(int id);

	@Query("SELECT a FROM Account a ORDER BY a.created ASC")
	List<Account> findAllOrderByCreated();

}
