package solutions.mlj.srovnejsito.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestParam;
import solutions.mlj.srovnejsito.model.dto.AccountDto;
import solutions.mlj.srovnejsito.model.dto.SingleSingOnDto;
import solutions.mlj.srovnejsito.model.dto.TokenDto;
import solutions.mlj.srovnejsito.model.dto.create.LoginDto;
import solutions.mlj.srovnejsito.model.dto.create.NewAccountDto;
import solutions.mlj.srovnejsito.model.enumerated.Role;

import java.util.List;

public interface AccountService {

	void resetPassword(String username);

	void changePassword(String password);

	AccountDto getCurrentAccount();

	AccountDto createAccount(NewAccountDto newAccountDto);

	@Secured(Role.MANAGER_STRING)
	AccountDto editAccount(NewAccountDto newAccountDto, int accountId);

	@Secured(Role.MANAGER_STRING)
	List<AccountDto> getAllAccounts();

	@Secured(Role.MANAGER_STRING)
	void deleteAccount(int accountId);

	ResponseEntity<TokenDto> singleSignOn(SingleSingOnDto request);

	TokenDto login(LoginDto loginDto);

	void sendNotification(String title, String message);

    HttpEntity<AccountDto> findAccount();

	void subscribeToNotifications(String token);

}
