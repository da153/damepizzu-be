package solutions.mlj.srovnejsito.logging.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import solutions.mlj.srovnejsito.logging.MessageHolder;
import solutions.mlj.srovnejsito.logging.model.Message;

import java.time.LocalDateTime;

@Service
public class MessageLoggingServiceImpl implements MessageLoggingService {

	private final Logger messageLogger = LoggerFactory.getLogger("message-log");

	@Value("${logging.message.enabled}")
	private boolean loggingEnabled;

	@Value("${logging.message.exception-only}")
	private boolean exceptionOnly;

	@Override
	public void handleRequest(String uri, String method, String urlParams) {
		Message message = new Message();
		message.setRequestTime(LocalDateTime.now());
		message.setUri(uri);
		message.setMethod(method);
		message.setUrlParams(urlParams);

		Authentication auth = SecurityContextHolder.getContext()
			.getAuthentication();
		if (auth != null) {
			message.setUser(auth.getPrincipal()
				.toString());
		}

		MessageHolder.set(message);
	}

	@Override
	public void handleResponse() {
		Message message = MessageHolder.get();
		message.setResponseTime(LocalDateTime.now());
	}

	@Override
	public void handleException(Exception exception) {
		Message message = MessageHolder.get();
		String exceptionName = exception.getClass()
			.getCanonicalName();
		String exceptionMessage = exception.getMessage();

		message.setException(exceptionName);
		message.setExceptionMessage(exceptionMessage);
		handleResponse();
	}

	@Override
	public void setRequestResponse(String request, String response) {
		Message message = MessageHolder.get();
		if (message != null && StringUtils.hasLength(request)) {
			message.setRequestBody(request);
		}
		if (message != null && StringUtils.hasLength(response)) {
			message.setResponseBody(response);
		}
	}

	@Override
	public void fileLog() {
		Message message = MessageHolder.get();
		if (message != null && loggingEnabled) {
			if (!exceptionOnly || StringUtils.hasText(message.getException())) {
				messageLogger.info(message.toString());
			}
		}
	}

}

