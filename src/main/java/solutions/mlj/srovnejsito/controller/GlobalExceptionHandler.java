package solutions.mlj.srovnejsito.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import solutions.mlj.srovnejsito.exception.model.Rfc7807Error;
import solutions.mlj.srovnejsito.exception.model.ValidationException;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler({ValidationException.class})
	public final ResponseEntity<?> validationException(ValidationException ex, WebRequest request) {
		return ResponseEntity.status(400)
			.body(ex.getRfc7807Error());
	}

	@ExceptionHandler({Exception.class})
	public final ResponseEntity<?> handleGeneralException(Exception ex, WebRequest request) {
		ex.printStackTrace();
		return ResponseEntity.status(400)
			.body(Rfc7807Error.parse(ex));
	}

	/**
	 * Provides handling for exceptions throughout this service.
	 */
	@ExceptionHandler({BadCredentialsException.class})
	public final ResponseEntity<?> handleException(Exception ex, WebRequest request) {
		HttpStatus status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status)
			.build();
	}

}
