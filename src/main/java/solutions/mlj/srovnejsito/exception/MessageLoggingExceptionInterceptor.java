package solutions.mlj.srovnejsito.exception;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import solutions.mlj.srovnejsito.logging.service.MessageLoggingService;
import solutions.mlj.srovnejsito.model.dto.ExceptionDto;

import java.net.ConnectException;
import java.nio.file.AccessDeniedException;

@ControllerAdvice
@AllArgsConstructor
@Slf4j
public class MessageLoggingExceptionInterceptor {

	private MessageLoggingService messageLoggingService;

	@ExceptionHandler(RuntimeException.class)
	@ResponseBody
	public ResponseEntity<ExceptionDto> generalException(RuntimeException e) {
		messageLoggingService.handleException(e);
		ExceptionDto exceptionDto = ExceptionDto.fromException(e);
		log.error(ExceptionUtils.extractExceptionInfo(e));
		return new ResponseEntity<>(exceptionDto, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseBody
	public ResponseEntity<ExceptionDto> validationException(MethodArgumentNotValidException e) {
		messageLoggingService.handleException(e);
		StringBuilder stringBuilder = new StringBuilder();
		e.getBindingResult()
			.getAllErrors()
			.stream()
			.map(error -> error.getDefaultMessage() + " ")
			.forEach(stringBuilder::append);
		String validationResult = stringBuilder.toString();
		ExceptionDto exceptionDto = new ExceptionDto(validationResult);
		exceptionDto.setPropagateToUser(true);
		//log.error(validationResult + "\n" + ExceptionUtils.extractExceptionInfo(e));
		return new ResponseEntity<>(exceptionDto, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody
	public ResponseEntity<ExceptionDto> illegalArgumentException(IllegalArgumentException e) {
		messageLoggingService.handleException(e);
		ExceptionDto exceptionDto = ExceptionDto.fromException(e);
		//log.error(ExceptionUtils.extractExceptionInfo(e));
		return new ResponseEntity<>(exceptionDto, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(RestClientException.class)
	@ResponseBody
	public ResponseEntity<ExceptionDto> restClientException(RestClientException e) {
		messageLoggingService.handleException(e);
		ExceptionDto exceptionDto = ExceptionDto.fromException(e);
		HttpStatus status = HttpStatus.BAD_REQUEST;
		if (e.getCause() instanceof ConnectException) {
			status = HttpStatus.SERVICE_UNAVAILABLE;
		}
		log.error(ExceptionUtils.extractExceptionInfo(e));
		return new ResponseEntity<>(exceptionDto, status);
	}

	@ExceptionHandler(AccessDeniedException.class)
	@ResponseBody
	public ResponseEntity<ExceptionDto> accessDeniedException(AccessDeniedException e) {
		// TODO User
		String errorMessage = "user with role cannot access this resource.";
		ExceptionDto exceptionDto = new ExceptionDto(errorMessage);
		log.error(ExceptionUtils.extractExceptionInfo(e));
		return new ResponseEntity<>(exceptionDto, HttpStatus.UNAUTHORIZED);
	}

}
