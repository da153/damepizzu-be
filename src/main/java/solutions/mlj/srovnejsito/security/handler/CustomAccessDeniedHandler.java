package solutions.mlj.srovnejsito.security.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import solutions.mlj.srovnejsito.model.domain.ExceptionDto;
import solutions.mlj.srovnejsito.security.model.AuthenticatedUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        /*String uri = httpServletRequest.getRequestURI();
        //String userRole = AuthenticatedUser.getCurrent().getAuthorities().iterator().next().getAuthority();
        String errorMessage = String.format("User with role %s cannot access url %s !", userRole, uri);

        log.error(errorMessage);
        ExceptionDto exceptionDto = new ExceptionDto(errorMessage);
        String exceptionString = objectMapper.writeValueAsString(exceptionDto);

        httpServletResponse.setStatus(HttpStatus.FORBIDDEN.value());
        httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
        httpServletResponse.getOutputStream().println(exceptionString);

         */
	    httpServletResponse.setStatus(HttpStatus.FORBIDDEN.value());
    }

}
