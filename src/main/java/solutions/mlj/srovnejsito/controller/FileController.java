package solutions.mlj.srovnejsito.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import solutions.mlj.srovnejsito.model.dto.PageDto;
import solutions.mlj.srovnejsito.model.dto.file.FileDto;
import solutions.mlj.srovnejsito.model.enumerated.GeneraalSortType;

import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@Tag(name = "File", description = "file uopload")
@Validated
public interface FileController {


	@PostMapping(path = "/api/file", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "Upload file", description = "Upload file")
	FileDto uploadFile(@RequestParam MultipartFile file);

	@GetMapping("/api/file")
	@Operation(summary = "Get all files", description = "Get all files")
	PageDto<FileDto> getAllFiles(@Parameter(description = "Number of file page", example = "0") @RequestParam(value = "pageNumber", defaultValue = "0") Integer pageNumber,

	                             @Parameter(description = "Number of file per one page", example = "50") @RequestParam(value = "pageSize", defaultValue = "50") Integer pageSize,

	                             @Parameter(description = "Search query", example = "") @RequestParam(value = "search", required = false) String searchQuery,

	                             @Parameter(description = "Sort type ENUM", example = "ID_DESC") @RequestParam(value = "sort", defaultValue = "ID_DESC") GeneraalSortType sortType);

	@GetMapping("/api/file/{id}")
	@Operation(summary = "Get file", description = "Get file")
	FileDto getFile(@PathVariable UUID id);

	@DeleteMapping("/api/file/{id}")
	@Operation(summary = "Delete file", description = "Delete file")
	void deleteFile(@PathVariable UUID id);

	@GetMapping("/f/{id}")
	@Operation(summary = "Serve file", description = "Serve file")
	ResponseEntity<byte[]> serveFile(@PathVariable UUID id, HttpServletResponse response);

}
