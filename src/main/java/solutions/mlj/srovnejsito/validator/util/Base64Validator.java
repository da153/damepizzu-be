package solutions.mlj.srovnejsito.validator.util;

import java.util.regex.Pattern;

public class Base64Validator {

	private static final String WHITESPACE_REGEX = "\\s";

	private static final Pattern BASE64_PATTERN = Pattern.compile(
		"^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$");

	public static boolean isValidBase64(final String s) {
		final String sanitized = s.replaceAll(WHITESPACE_REGEX, "");
		return BASE64_PATTERN.matcher(sanitized)
			.matches();
	}

}
