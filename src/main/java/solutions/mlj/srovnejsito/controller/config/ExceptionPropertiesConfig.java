package solutions.mlj.srovnejsito.controller.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:exception.properties")
public class ExceptionPropertiesConfig {

	private final Environment env;

	public ExceptionPropertiesConfig(Environment env) {
		this.env = env;
	}

	public String getConfigValue(String configKey) {
		return env.getProperty(configKey);
	}

}
