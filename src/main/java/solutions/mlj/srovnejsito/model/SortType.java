package solutions.mlj.srovnejsito.model;

import org.springframework.data.domain.Sort;

import java.util.List;

public interface SortType {

	List<Sort.Order> getSortOrders();

	Sort.Direction getDirection();

}
