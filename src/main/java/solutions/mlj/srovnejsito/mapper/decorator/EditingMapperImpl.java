package solutions.mlj.srovnejsito.mapper.decorator;

import org.springframework.beans.factory.annotation.Autowired;
import solutions.mlj.srovnejsito.mapper.EditingMapper;
import solutions.mlj.srovnejsito.mapper.editor.Editor;

public class EditingMapperImpl<T> implements EditingMapper<T> {

	private Editor<T> editor;

	public void edit(T toEdit, T editBy) {
		editor.edit(toEdit, editBy);
	}

	@Autowired
	public void setOwnerEditor(Editor<T> editor) {
		this.editor = editor;
	}

}
