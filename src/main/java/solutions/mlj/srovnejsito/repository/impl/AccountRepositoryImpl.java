package solutions.mlj.srovnejsito.repository.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import solutions.mlj.srovnejsito.model.domain.Account;
import solutions.mlj.srovnejsito.repository.AccountRepository;
import solutions.mlj.srovnejsito.repository.engine.AccountRepositoryEngine;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class AccountRepositoryImpl extends AbstractRepository<Account, Integer> implements AccountRepository {

    private final AccountRepositoryEngine accountRepositoryEngine;

    @Override
    public Account getByUsername(String username) {
        return accountRepositoryEngine.findByUsername(username);
    }

	@Override
	public Account getByEmail(String email) {
		return accountRepositoryEngine.findByEmail(email);
	}

	@Override
    public List<Account> getAllAccounts() {
        return accountRepositoryEngine.findAllOrderByCreated();
    }

    @Override
    protected CrudRepository<Account, Integer> getRepositoryEngine() {
        return accountRepositoryEngine;
    }

}
