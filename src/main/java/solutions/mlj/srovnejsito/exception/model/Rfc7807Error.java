package solutions.mlj.srovnejsito.exception.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

@Data
@With
@NoArgsConstructor
@AllArgsConstructor
public class Rfc7807Error {

	private HttpStatus code;

	private String title;

	private String detail;

	private String instance;

	private List<InvalidParam> invalidParams = new ArrayList<>();

	public static Rfc7807Error parse(Exception e) {
		return new Rfc7807Error().withCode(HttpStatus.BAD_REQUEST)
			.withDetail(e.getMessage())
			.withInstance(e.getClass()
				.getName());
	}

	public void addParam(InvalidParam invalidParam) {
		this.getInvalidParams()
			.add(invalidParam);
	}

	public Rfc7807Error addParams(List<InvalidParam> invalidParams) {
		this.getInvalidParams()
			.addAll(invalidParams);
		return this;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	@Builder
	public static class InvalidParam {

		private String name;

		private String reason;

	}

}
