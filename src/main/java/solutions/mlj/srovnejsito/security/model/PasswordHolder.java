package solutions.mlj.srovnejsito.security.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PasswordHolder {

    private String rawPassword;

    private String hashedPassword;

}
