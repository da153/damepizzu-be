package solutions.mlj.srovnejsito.controller.impl;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;
import solutions.mlj.srovnejsito.controller.AccountController;
import solutions.mlj.srovnejsito.model.dto.AccountDto;
import solutions.mlj.srovnejsito.model.dto.FirebaseTokenDto;
import solutions.mlj.srovnejsito.model.dto.SingleSingOnDto;
import solutions.mlj.srovnejsito.model.dto.TokenDto;
import solutions.mlj.srovnejsito.model.dto.create.LoginDto;
import solutions.mlj.srovnejsito.model.dto.create.NewAccountDto;
import solutions.mlj.srovnejsito.security.model.AuthenticatedUser;
import solutions.mlj.srovnejsito.service.AccountService;
import solutions.mlj.srovnejsito.service.FirebaseMessagingService;

import java.util.List;

@RestController
@AllArgsConstructor
public class AccountControllerImpl implements AccountController {

    private AccountService accountService;

    @Override
    public void resetPassword(String username) {
        accountService.resetPassword(username);
    }

    @Override
    public void changePassword(String password) {
        accountService.changePassword(password);
    }

	@Override
	public void sendNotification(String title, String message) {
		accountService.sendNotification(title, message);
	}

	@Override
	public void subscribeToNotifications(FirebaseTokenDto tokenDto) {
		accountService.subscribeToNotifications(tokenDto.getToken());
	}

	@Override
    public AccountDto getCurrentAccount() {
        return accountService.getCurrentAccount();
    }

    @Override
    public AccountDto createAccount(NewAccountDto newAccountDto) {
        return accountService.createAccount(newAccountDto);
    }

    @Override
    public AccountDto editAccount(NewAccountDto newAccountDto, Integer accountId) {
        return accountService.editAccount(newAccountDto, accountId);
    }

    @Override
    public List<AccountDto> getAllAccounts() {
        return accountService.getAllAccounts();
    }

    @Override
    public void deleteAccount(Integer accountId) {
        accountService.deleteAccount(accountId);
    }

	@Override
	public TokenDto login(LoginDto loginDto) {
		return accountService.login(loginDto);
	}

	@Override
	public HttpEntity<TokenDto> singleSignOn(SingleSingOnDto request) {
		return accountService.singleSignOn(request);
	}

	@Override
    public void fakeLogout() {
        throw new UnsupportedOperationException("This method should never be called, replaced by Spring Security. For Open API purposes.");
    }

}
