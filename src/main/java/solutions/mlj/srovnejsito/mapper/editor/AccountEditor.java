package solutions.mlj.srovnejsito.mapper.editor;

import org.springframework.stereotype.Component;
import solutions.mlj.srovnejsito.model.domain.Account;

@Component
public class AccountEditor implements Editor<Account> {

    @Override
    public void edit(Account toEdit, Account editBy) {
        toEdit.setUsername(editBy.getUsername());
        toEdit.setRole(editBy.getRole());

        toEdit.setFirstName(editBy.getFirstName());
        toEdit.setLastName(editBy.getLastName());
        toEdit.setEmail(editBy.getEmail());
        toEdit.setPhone(editBy.getPhone());
    }

}
