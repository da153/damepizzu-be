package solutions.mlj.srovnejsito.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;
import solutions.mlj.srovnejsito.model.enumerated.Role;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class AccountDto {

    @Schema(description = "ID of the User", example = "45", required = true)
    private Integer id;

    @Schema(description = "Username of the User", example = "Jan", required = true)
    private String username;

    @Schema(description = "Role of the User", example = "MANAGER", required = true)
    private Role role;

    @Schema(description = "Date and time of Account creation", example = "2020-01-30T10:15:30", required = true, implementation = String.class)
    private LocalDateTime created;

    @Schema(description = "Date and time of last login", example = "2020-01-29T10:26:06", implementation = String.class)
    private LocalDateTime lastLogin;

    @Schema(description = "First name of the User", example = "Jan", required = true)
    private String firstName;

    @Schema(description = "Last name of the User", example = "Novák", required = true)
    private String lastName;

    @Schema(description = "Email of the User", example = "jan.novak@seznam.cz", required = true)
    private String email;

    @Schema(description = "Phone of the User", example = "777568951")
    private String phone;

	@Schema(description = "Firebase token of the User", example = "")
	private String firebaseToken;

}
