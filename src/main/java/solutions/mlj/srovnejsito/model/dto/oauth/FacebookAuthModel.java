package solutions.mlj.srovnejsito.model.dto.oauth;

import lombok.Data;

@Data
public class FacebookAuthModel {
	private String authToken;
}
