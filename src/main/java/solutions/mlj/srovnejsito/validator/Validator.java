package solutions.mlj.srovnejsito.validator;

import java.util.List;

public interface Validator<T> {

	void validateBeforeCreation(T object);

	void validateBeforeEditing(T object);

	default void validateBeforeOrdering(List<T> object) {}

}
