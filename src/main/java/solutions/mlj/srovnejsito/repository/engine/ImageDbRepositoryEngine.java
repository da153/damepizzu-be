package solutions.mlj.srovnejsito.repository.engine;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import solutions.mlj.srovnejsito.model.domain.File;

import java.util.UUID;


public interface ImageDbRepositoryEngine extends CrudRepository<File, UUID> {

	@Query("SELECT f FROM File f")
	Page<File> findAll(Pageable pageable);

}
