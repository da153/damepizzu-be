package solutions.mlj.srovnejsito.repository.minio;

import io.minio.ObjectStat;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import solutions.mlj.srovnejsito.repository.FileStorage;
import solutions.mlj.srovnejsito.service.MinioRepositoryService;
import solutions.mlj.srovnejsito.util.Tuple;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
@AllArgsConstructor
public class MinioRepository implements FileStorage {

	private final MinioRepositoryService minioService;

	@Override
	public String save(String fileName, byte[] file) {
		return "";
	}

	@Override
	public ObjectStat save(String fileName, MultipartFile file) {
		Path path = Path.of(Objects.requireNonNull(file.getOriginalFilename()));
		Map<String, String> header = new HashMap<>();
		header.put("X-Incident-Id", "C918371984");
		try {
			minioService.upload(path, file.getInputStream(), file.getContentType(), header);
		} catch (RuntimeException e) {
			throw new IllegalStateException("The file cannot be upload on the internal storage. Please retry later", e);
		} catch (IOException e) {
			throw new IllegalStateException("The file cannot be read", e);
		}
		return minioService.getMetadata(path);
	}

	@Override
	public void delete(String filepath) {
		minioService.remove(Path.of(filepath));
	}

	@Override
	public byte[] get(String filepath) {
		InputStream inputStream = minioService.get(Path.of(filepath));
		ObjectStat metadata = minioService.getMetadata(Path.of(filepath));
		try {
			return inputStream.readAllBytes();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Tuple<byte[], ObjectStat> getWithMetadata(String filepath) {
		InputStream inputStream = minioService.get(Path.of(filepath));
		ObjectStat metadata = minioService.getMetadata(Path.of(filepath));

		byte[] bytes = null;
		try {
			bytes = inputStream.readAllBytes();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new Tuple<>(bytes, metadata);
	}

}
