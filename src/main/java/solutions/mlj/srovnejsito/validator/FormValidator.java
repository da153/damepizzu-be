package solutions.mlj.srovnejsito.validator;

import java.util.Map;


public interface FormValidator {

	void validateRequiredInputs(Map<String, String> fromFields);

	void validateByStep(Map<String, String> fields, Integer id);

}
