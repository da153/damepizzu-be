package solutions.mlj.srovnejsito.mapper;

import org.springframework.data.domain.Page;
import solutions.mlj.srovnejsito.model.dto.PageDto;

public class PageMetaInfoMapper {

	public static <T> PageDto<T> mapPageMetaInfo(Page<?> page) {
		PageDto<T> pageDto = new PageDto<>();
		pageDto.setPageNumber(page.getNumber());
		pageDto.setTotalPages(page.getTotalPages());
		pageDto.setHasNextPage(page.hasNext());
		pageDto.setElementsPerPage(page.getPageable()
			.getPageSize());
		pageDto.setNumberOfElements(page.getNumberOfElements());
		pageDto.setTotalNumberOfElements((int) page.getTotalElements());
		return pageDto;
	}

}
