package solutions.mlj.srovnejsito.exception.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ValidationException extends IllegalArgumentException {

	private Rfc7807Error rfc7807Error;

	public ValidationException() {
	}

	public ValidationException(Rfc7807Error rfc7807Error) {
		this.rfc7807Error = rfc7807Error;
	}

}
