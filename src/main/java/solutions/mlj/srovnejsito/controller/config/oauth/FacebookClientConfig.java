package solutions.mlj.srovnejsito.controller.config.oauth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Configuration
public class FacebookClientConfig {

	@Value("${oauth.facebook}")
	private String facebook;

	@Bean
	public FacebookClient createProductMenuApiClient(){
		Retrofit retrofit = new Retrofit.Builder()
			.baseUrl(facebook)
			.addConverterFactory(GsonConverterFactory.create())
			.build();
		return retrofit.create(FacebookClient.class);
	}
}
