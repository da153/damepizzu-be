package solutions.mlj.srovnejsito.repository.impl;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import solutions.mlj.srovnejsito.repository.BaseRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<T, V> implements BaseRepository<T, V> {

	private static final String ALL_SIGN = "%";

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public T getById(V id) {
		Optional<T> object = getRepositoryEngine().findById(id);
		return object.orElse(null);
	}

	@Override
	public List<T> findAll() {
		return (List<T>) getRepositoryEngine().findAll();
	}

	@Override
	public T getNotNull(V id) {
		T object = getById(id);
		Assert.notNull(object, "Cannot find desired object with id: " + id);
		return object;
	}

	@Transactional
	@Override
	public T save(T toSave) {
		T savedObject = getRepositoryEngine().save(toSave);
		entityManager.flush();
		entityManager.refresh(savedObject);
		return savedObject;
	}

	@Transactional
	@Override
	public void deleteById(V id) {
		entityManager.flush();
		entityManager.clear();
		getRepositoryEngine().deleteById(id);
	}

	@Override
	public void delete(T object) {
		getRepositoryEngine().delete(object);
	}

	@Override
	public void clear() {
		getRepositoryEngine().deleteAll();
	}

	@Transactional
	@Override
	public void clearCache() {
		entityManager.clear();
	}

	protected String formatSearchString(String searchString) {
		return StringUtils.hasText(searchString) ? ALL_SIGN + searchString + ALL_SIGN : ALL_SIGN;
	}

	protected abstract CrudRepository<T, V> getRepositoryEngine();

}
