package solutions.mlj.srovnejsito.validator;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Slf4j
@SuppressWarnings({"rawtypes"})
public class ValidationManager {

	private final List<Validator> validators;

	public <T> void validateBeforeCreation(T object) {
		Optional<Validator> validator = getValidatorOfType(object.getClass());
		validator.ifPresent(v -> v.validateBeforeCreation(object));
	}

	public <T> void validateBeforeEditing(T object) {
		Optional<Validator> validator = getValidatorOfType(object.getClass());
		validator.ifPresent(v -> v.validateBeforeEditing(object));
	}

	public <T> void validateBeforeOrdering(List<T> object, Class type) {
		Optional<Validator> validator = getValidatorOfType(type);
		validator.ifPresent(v -> v.validateBeforeOrdering(object));
	}

	private Optional<Validator> getValidatorOfType(Class type) {
		Optional<Validator> validator = validators.stream()
			.filter(v -> GenericTypeResolver.resolveTypeArgument(v.getClass(), Validator.class) == type)
			.findAny();
		if (validator.isEmpty()) {
			log.error("Cannot find validator for type " + type.getName() + ", validation process will be skipped.");
		}
		return validator;
	}

}
