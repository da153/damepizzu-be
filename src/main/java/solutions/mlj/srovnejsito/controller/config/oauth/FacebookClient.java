package solutions.mlj.srovnejsito.controller.config.oauth;

import org.springframework.stereotype.Component;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import solutions.mlj.srovnejsito.model.dto.oauth.FacebookUserModel;

import java.util.List;

public interface FacebookClient {

	@GET("/me")
	Call<FacebookUserModel> getFaceBookUser(@Query(value = "fields", encoded = true) String fields, @Query("access_token") String token);

}
