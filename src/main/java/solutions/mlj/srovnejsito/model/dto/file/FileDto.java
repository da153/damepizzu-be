package solutions.mlj.srovnejsito.model.dto.file;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileDto {

	public UUID id;

	public String name;

	public String type;

	public Long size;

	public String url;

}
