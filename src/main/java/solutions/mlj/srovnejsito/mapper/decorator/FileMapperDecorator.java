package solutions.mlj.srovnejsito.mapper.decorator;

import io.minio.ObjectStat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import solutions.mlj.srovnejsito.mapper.FileMapper;
import solutions.mlj.srovnejsito.model.domain.File;
import solutions.mlj.srovnejsito.model.dto.file.FileDto;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class FileMapperDecorator extends EditingMapperImpl<File> implements FileMapper {

	private FileMapper fileMapper;

	@Override
	public FileDto entityToDto(File file) {
		FileDto fileDto = fileMapper.entityToDto(file);
		if (Objects.isNull(file) || Objects.isNull(fileDto)) {
			return null;
		}
		fileDto.setUrl(String.format("/f/%s", fileDto.getId()));
		return fileDto;
	}

	@Override
	public List<FileDto> entitiesToDto(List<File> files) {
		return files.stream()
			.map(this::entityToDto)
			.collect(Collectors.toList());
	}

	@Override
	public File fileAndMetadataToDbEntity(MultipartFile file, ObjectStat metadata) {
		File dbFile = new File();
		dbFile.setName(metadata.name());
		dbFile.setSize(file.getSize());
		dbFile.setETag(metadata.etag());
		dbFile.getFileType(Objects.requireNonNull(file.getContentType()));
		dbFile.setHash(metadata.hashCode());
		return dbFile;
	}

	@Autowired
	public void setFileMapper(FileMapper fileMapper) {
		this.fileMapper = fileMapper;
	}

}
