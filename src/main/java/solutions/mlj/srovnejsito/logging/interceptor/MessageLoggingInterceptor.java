package solutions.mlj.srovnejsito.logging.interceptor;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import solutions.mlj.srovnejsito.logging.service.MessageLoggingService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

@Component
@AllArgsConstructor
public class MessageLoggingInterceptor extends HandlerInterceptorAdapter {

	private MessageLoggingService messageLoggingService;

	@Override
	public boolean preHandle(HttpServletRequest request,
	                         HttpServletResponse response,
	                         Object handler) throws Exception {
		String uri = request.getRequestURI();
		String method = request.getMethod();
		String queryString = request.getQueryString();
		if (queryString != null) {
			queryString = URLDecoder.decode(queryString, StandardCharsets.UTF_8);
		}

		messageLoggingService.handleRequest(uri, method, queryString);
		return true;
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
	                            HttpServletResponse response,
	                            Object handler,
	                            Exception ex) throws Exception {
		messageLoggingService.handleResponse();
	}

}
