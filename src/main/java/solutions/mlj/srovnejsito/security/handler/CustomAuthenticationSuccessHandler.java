package solutions.mlj.srovnejsito.security.handler;

import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import solutions.mlj.srovnejsito.security.AuthenticatedUserService;
import solutions.mlj.srovnejsito.security.model.AuthenticatedUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@AllArgsConstructor
public class CustomAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private AuthenticatedUserService authenticatedUserService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        int userId = AuthenticatedUser.getCurrent().getAccountId();
        authenticatedUserService.updateLastLoginTime(userId);

        super.clearAuthenticationAttributes(httpServletRequest);
    }

}
