package solutions.mlj.srovnejsito.service;

import io.minio.ObjectStat;
import org.springframework.web.multipart.MultipartFile;
import solutions.mlj.srovnejsito.model.dto.PageDto;
import solutions.mlj.srovnejsito.model.dto.file.FileDto;
import solutions.mlj.srovnejsito.model.enumerated.GeneraalSortType;
import solutions.mlj.srovnejsito.util.Tuple;

import java.util.UUID;

public interface FileService {

	FileDto getFile(UUID id);

	FileDto uploadFile(MultipartFile file);

	void deleteFile(UUID id);

	PageDto<FileDto> getAllFiles(Integer pageNumber, Integer pageSize, String searchQuery, GeneraalSortType sortType);

	Tuple<byte[], ObjectStat> serveFile(UUID id);

}
