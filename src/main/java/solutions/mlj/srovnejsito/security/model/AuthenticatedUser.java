package solutions.mlj.srovnejsito.security.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.context.SecurityContextHolder;
import solutions.mlj.srovnejsito.model.domain.Account;
import solutions.mlj.srovnejsito.model.dto.oauth.UserPrincipal;

import java.util.*;

@Getter
@Setter
public class AuthenticatedUser {

	private final int accountId;
	private final String email;

    private static final String ANONYMOUS_USERNAME = "anonymousUser";
    private static final String ANONYMOUS_ROLE = "ROLE_CASHIER";

	public AuthenticatedUser(int id, String email) {
		this.accountId = id;
		this.email = email;
	}

	public static AuthenticatedUser getCurrent() {
        try {
	        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext()
		        .getAuthentication()
		        .getPrincipal();
            Objects.requireNonNull(userPrincipal);
            return new AuthenticatedUser(userPrincipal.getAccount().getId(), userPrincipal.getAccount().getEmail());
        } catch (Exception e) {
            return new AuthenticatedUser(0, null);
        }
    }

    public static boolean isAnonymous() {
        AuthenticatedUser currentUser = AuthenticatedUser.getCurrent();
        return currentUser.getAccountId() != 0;
    }

}
