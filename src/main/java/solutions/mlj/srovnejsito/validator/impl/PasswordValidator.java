package solutions.mlj.srovnejsito.validator.impl;

import org.springframework.stereotype.Component;
import solutions.mlj.srovnejsito.exception.model.UserPropagableException;
import solutions.mlj.srovnejsito.model.dto.create.NewPasswordDto;
import solutions.mlj.srovnejsito.validator.Validator;

@Component
public class PasswordValidator implements Validator<NewPasswordDto> {

	@Override
	public void validateBeforeCreation(NewPasswordDto passwordDto) {
			String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}";
			if (!passwordDto.getPassword().matches(pattern)) {
				throw new UserPropagableException("Must Contain One Uppercase, One Lowercase, One Number and be length min 6 chars!");
			}
	}

	@Override
	public void validateBeforeEditing(NewPasswordDto object) {

	}

}
