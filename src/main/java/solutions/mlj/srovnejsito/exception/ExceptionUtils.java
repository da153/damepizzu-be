package solutions.mlj.srovnejsito.exception;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ExceptionUtils {

	private static final String STRING_DELIMITER = "\n\t\t\t";

	public static String extractExceptionInfo(Exception e) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(e.getMessage());
		StackTraceElement[] stackTraceElements = e.getStackTrace();
		if (stackTraceElements != null) {
			stringBuilder.append(STRING_DELIMITER);
			String stackTrace = Arrays.stream(stackTraceElements)
				.map(StackTraceElement::toString)
				.collect(Collectors.joining(STRING_DELIMITER));
			stringBuilder.append(stackTrace);
		}
		return stringBuilder.toString();
	}

}
