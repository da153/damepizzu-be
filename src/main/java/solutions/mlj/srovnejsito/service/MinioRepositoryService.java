package solutions.mlj.srovnejsito.service;

import io.minio.MinioClient;
import io.minio.ObjectStat;
import io.minio.PutObjectOptions;
import io.minio.Result;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.XmlParserException;
import io.minio.messages.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import solutions.mlj.srovnejsito.controller.config.MinioConfigurationProperties;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class MinioRepositoryService {

	@Autowired
	private MinioClient minioClient;

	@Autowired
	private MinioConfigurationProperties configurationProperties;


	/**
	 * List all objects at root of the bucket
	 *
	 * @return List of items
	 */
	public List<Item> list() {
		Iterable<Result<Item>> myObjects = minioClient.listObjects(configurationProperties.getBucket(), "", false);
		return getItems(myObjects);
	}

	/**
	 * List all objects at root of the bucket
	 *
	 * @return List of items
	 */
	public List<Item> fullList() {
		try {
			Iterable<Result<Item>> myObjects = minioClient.listObjects(configurationProperties.getBucket());
			return getItems(myObjects);
		} catch (XmlParserException e) {
			throw new RuntimeException("Error while fetching files in Minio", e);
		}
	}

	/**
	 * List all objects with the prefix given in parameter for the bucket.
	 * Simulate a folder hierarchy. Objects within folders (i.e. all objects which match the pattern {@code {prefix}/{objectName}/...}) are not returned
	 *
	 * @param path Prefix of seeked list of object
	 * @return List of items
	 */
	public List<Item> list(Path path) {

		Iterable<Result<Item>> myObjects = minioClient.listObjects(configurationProperties.getBucket(),
			path.toString(),
			false
		);
		return getItems(myObjects);
	}

	/**
	 * List all objects with the prefix given in parameter for the bucket
	 * <p>
	 * All objects, even those which are in a folder are returned.
	 *
	 * @param path Prefix of seeked list of object
	 * @return List of items
	 */
	public List<Item> getFullList(Path path) {
		try {
			Iterable<Result<Item>> myObjects = minioClient.listObjects(configurationProperties.getBucket(),
				path.toString()
			);
			return getItems(myObjects);
		} catch (XmlParserException e) {
			throw new RuntimeException("Error while fetching files in Minio", e);
		}
	}

	/**
	 * Utility method which map results to items and return a list
	 *
	 * @param myObjects Iterable of results
	 * @return List of items
	 */
	private List<Item> getItems(Iterable<Result<Item>> myObjects) {
		return StreamSupport.stream(myObjects.spliterator(), true)
			.map(itemResult -> {
				try {
					return itemResult.get();
				} catch (InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException | IOException | InvalidKeyException | XmlParserException | InvalidResponseException | ErrorResponseException | InternalException e) {
					throw new RuntimeException("Error while parsing list of objects", e);
				}
			})
			.collect(Collectors.toList());
	}

	/**
	 * Get an object from Minio
	 *
	 * @param path Path with prefix to the object. Object name must be included.
	 * @return The object as an InputStream
	 */
	public InputStream get(Path path) {
		try {
			return minioClient.getObject(configurationProperties.getBucket(), path.toString());
		} catch (XmlParserException | InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException | IOException | InvalidKeyException | ErrorResponseException | InternalException | InvalidResponseException e) {
			throw new RuntimeException("Error while fetching files in Minio", e);
		}
	}

	/**
	 * Get metadata of an object from Minio
	 *
	 * @param path Path with prefix to the object. Object name must be included.
	 * @return Metadata of the  object
	 */
	public ObjectStat getMetadata(Path path) {
		try {
			return minioClient.statObject(configurationProperties.getBucket(), path.toString());
		} catch (XmlParserException | InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException | IOException | InvalidKeyException | ErrorResponseException | InternalException | InvalidResponseException e) {
			throw new RuntimeException("Error while fetching files in Minio", e);
		}
	}

	/**
	 * Get metadata for multiples objects from Minio
	 *
	 * @param paths Paths of all objects with prefix. Objects names must be included.
	 * @return A map where all paths are keys and metadatas are values
	 */
	public Map<Path, ObjectStat> getMetadata(Iterable<Path> paths) {
		return StreamSupport.stream(paths.spliterator(), false)
			.map(path -> {
				try {
					return new HashMap.SimpleEntry<>(path,
						minioClient.statObject(configurationProperties.getBucket(), path.toString())
					);
				} catch (InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException | IOException | InvalidKeyException | XmlParserException | ErrorResponseException | InternalException | InvalidResponseException e) {
					throw new RuntimeException("Error while parsing list of objects", e);
				}
			})
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	/**
	 * Get a file from Minio, and save it in the {@code fileName} file
	 *
	 * @param source   Path with prefix to the object. Object name must be included.
	 * @param fileName Filename
	 */
	public void getAndSave(Path source, String fileName) {
		try {
			minioClient.getObject(configurationProperties.getBucket(), source.toString(), fileName);
		} catch (XmlParserException | InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException | IOException | InvalidKeyException | ErrorResponseException | InternalException | InvalidResponseException e) {
			throw new RuntimeException("Error while fetching files in Minio", e);
		}
	}

	/**
	 * Upload a file to Minio
	 *
	 * @param source  Path with prefix to the object. Object name must be included.
	 * @param file    File as an inputstream
	 * @param headers Additional headers to put on the file. The map MUST be mutable. All custom headers will start with 'x-amz-meta-' prefix when fetched with {@code getMetadata()} method.
	 */
	public void upload(Path source, InputStream file, Map<String, String> headers) {
		try {
			PutObjectOptions options = new PutObjectOptions(file.available(), -1);
			options.setHeaders(headers);
			minioClient.putObject(configurationProperties.getBucket(), source.toString(), file, options);
		} catch (XmlParserException | InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException | IOException | InvalidKeyException | ErrorResponseException | InternalException | InvalidResponseException e) {
			throw new RuntimeException("Error while fetching files in Minio", e);
		}
	}

	public void upload(Path source, InputStream file) {
		try {
			minioClient.putObject(configurationProperties.getBucket(),
				source.toString(),
				file,
				new PutObjectOptions(file.available(), -1)
			);
		} catch (XmlParserException | InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException | IOException | InvalidKeyException | ErrorResponseException | InternalException | InvalidResponseException e) {
			throw new RuntimeException("Error while fetching files in Minio", e);
		}
	}

	public void upload(Path source, InputStream file, String contentType, Map<String, String> headers) {
		try {
			PutObjectOptions options = new PutObjectOptions(file.available(), -1);
			options.setContentType(contentType);
			options.setHeaders(headers);
			minioClient.putObject(configurationProperties.getBucket(), source.toString(), file, options);
		} catch (XmlParserException | InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException | IOException | InvalidKeyException | ErrorResponseException | InternalException | InvalidResponseException e) {
			throw new RuntimeException("Error while fetching files in Minio");
		}
	}

	/**
	 * Upload a file to Minio
	 *
	 * @param source      Path with prefix to the object. Object name must be included.
	 * @param file        File as an inputstream
	 * @param contentType MIME type for the object
	 */
	public void upload(Path source, InputStream file, String contentType) {
		try {
			PutObjectOptions options = new PutObjectOptions(file.available(), -1);
			options.setContentType(contentType);
			minioClient.putObject(configurationProperties.getBucket(), source.toString(), file, options);
		} catch (XmlParserException | InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException | IOException | InvalidKeyException | ErrorResponseException | InternalException | InvalidResponseException e) {
			throw new RuntimeException("Error while fetching files in Minio", e);
		}
	}

	/**
	 * Upload a file to Minio
	 * upload file bigger than Xmx size
	 *
	 * @param source Path with prefix to the object. Object name must be included.
	 * @param file   File as an Filename
	 */
	public void upload(Path source, File file, int partSize) {
		try {
			minioClient.putObject(configurationProperties.getBucket(),
				source.toString(),
				file.getAbsolutePath(),
				new PutObjectOptions(Files.size(file.toPath()), partSize)
			);
		} catch (XmlParserException | InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException | IOException | InvalidKeyException | ErrorResponseException | InternalException | InvalidResponseException e) {
			throw new RuntimeException("Error while fetching files in Minio", e);
		}
	}


	/**
	 * Remove a file to Minio
	 *
	 * @param source Path with prefix to the object. Object name must be included.
	 */
	public void remove(Path source) {
		try {
			minioClient.removeObject(configurationProperties.getBucket(), source.toString());
		} catch (XmlParserException | InvalidBucketNameException | NoSuchAlgorithmException | InsufficientDataException | IOException | InvalidKeyException | ErrorResponseException | InternalException | InvalidResponseException e) {
			throw new RuntimeException("Error while fetching files in Minio");
		}
	}

}
