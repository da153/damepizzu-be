package solutions.mlj.srovnejsito.mapper;

import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import solutions.mlj.srovnejsito.mapper.decorator.AccountMapperDecorator;
import solutions.mlj.srovnejsito.model.domain.Account;
import solutions.mlj.srovnejsito.model.dto.AccountDto;
import solutions.mlj.srovnejsito.model.dto.create.NewAccountDto;

import java.util.List;

@Mapper(componentModel = "spring")
@DecoratedWith(AccountMapperDecorator.class)
public interface AccountMapper extends EditingMapper<Account> {

    public AccountDto accountToAccountDto(Account account);

    public List<AccountDto> accountsToAccountDtos(List<Account> accounts);

    public Account newAccountDtoToAccount(NewAccountDto newAccountDto);

}
