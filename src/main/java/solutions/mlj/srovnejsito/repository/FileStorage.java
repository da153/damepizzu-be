package solutions.mlj.srovnejsito.repository;

import io.minio.ObjectStat;
import org.springframework.web.multipart.MultipartFile;
import solutions.mlj.srovnejsito.util.Tuple;

public interface FileStorage {

	String save(String fileName, byte[] file);

	ObjectStat save(String fileName, MultipartFile file);

	void delete(String filepath);

	byte[] get(String filepath);

	Tuple<byte[], ObjectStat> getWithMetadata(String filepath);

}
