package solutions.mlj.srovnejsito.repository;

import java.util.List;

public interface BaseRepository<T, V> extends AutoSelectRepository<T> {

	T getById(V id);

	T getNotNull(V id);

	T save(T object);

	void deleteById(V id);

	void delete(T object);

	void clearCache();

	List<T> findAll();

	void clear();

}
