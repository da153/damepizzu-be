package solutions.mlj.srovnejsito.mapper.editor;

public interface Editor<T> {

	void edit(T toEdit, T editBy);

}
