package solutions.mlj.srovnejsito.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FirebaseTokenDto {
	public String token;
}
