package solutions.mlj.srovnejsito.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import solutions.mlj.srovnejsito.model.dto.AccountDto;
import solutions.mlj.srovnejsito.model.dto.FirebaseTokenDto;
import solutions.mlj.srovnejsito.model.dto.SingleSingOnDto;
import solutions.mlj.srovnejsito.model.dto.TokenDto;
import solutions.mlj.srovnejsito.model.dto.create.LoginDto;
import solutions.mlj.srovnejsito.model.dto.create.NewAccountDto;

import javax.validation.Valid;
import java.util.List;

@Tag(name = "Account", description = "API for Account management.")
public interface AccountController {

	@Operation(summary = "Reset forgotten password", description = "System will generate new random password and send it to " + "user's email.")
	@PostMapping("/api/public/account/password/reset")
	public void resetPassword(@Parameter(description = "Username of the user", example = "jan.novak@seznam.cz", required = true) @RequestParam("username") String username);

	@Operation(summary = "Change password", description = "User must be logged in to change password. Can be used with Internal accounts only.")
	@PostMapping("/api/account/password/change")
	public void changePassword(@Parameter(description = "New password, 6 chars min", example = "123aaa", required = true) @RequestParam("password") String password);

	@PostMapping("/api/account/push-notification")
	public void sendNotification(@RequestParam("title") String title, @RequestParam("message") String message);

	@Operation(summary = "Send push notification", description = "")
	@PostMapping("/api/account/notifications")
	public void subscribeToNotifications(@Parameter(description = "Firebase token", example = "asdklaksdlasdkl", required = true) @RequestBody @Valid FirebaseTokenDto tokenDto);


	@Operation(summary = "Get current Account", description = "User must be logged in to fetch current Account.")
	@GetMapping("/api/account/me")
	public AccountDto getCurrentAccount();

	@Operation(summary = "Create new Account", description = "Creates new account for a user.")
	@PostMapping("/api/public/account/new")
	public AccountDto createAccount(@RequestBody @Valid NewAccountDto newAccountDto);

	@Operation(summary = "Edit Account", description = "Edits existing user's Account.")
	@PostMapping("/api/account/{accountId}/edit")
	public AccountDto editAccount(@RequestBody @Valid NewAccountDto newAccountDto,
	                              @PathVariable("accountId") Integer accountId);

	@Operation(summary = "Get all Accounts", description = "Gets Accounts of all users.")
	@GetMapping("/api/account/all")
	public List<AccountDto> getAllAccounts();

	@Operation(summary = "Delete Account", description = "Deletes user's Account. User cannot delete currently logged-in Account.")
	@DeleteMapping("/api/account/{accountId}")
	public void deleteAccount(@PathVariable("accountId") Integer accountId);

	@Operation(summary = "Login to account")
	@PostMapping("/api/account/login")
	TokenDto login(@RequestBody() LoginDto loginDto);

	@Operation(summary = "Single sign on using 3P provider")
	@PostMapping("/api/account/single-sign-on")
	HttpEntity<TokenDto> singleSignOn(@RequestBody() SingleSingOnDto body);

	@Operation(summary = "Logout user")
	@PostMapping("/logout")
	public void fakeLogout();

}
