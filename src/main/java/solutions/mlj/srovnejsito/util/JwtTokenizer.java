package solutions.mlj.srovnejsito.util;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import solutions.mlj.srovnejsito.controller.config.JwtConfig;
import solutions.mlj.srovnejsito.exception.model.UserPropagableException;
import solutions.mlj.srovnejsito.model.domain.Account;

import java.util.Date;

@Component
@AllArgsConstructor
public class JwtTokenizer {

	private final JwtConfig config;

	public String tokenizeChangePasswordToken(String id) {
		return JWT.create()
			.withSubject(id)
			.withExpiresAt(new Date(System.currentTimeMillis() + config.getExpiration()))
			.sign(Algorithm.HMAC256(config.getSecret()));
	}

	public String generateTokenForAccount(Account account)  {
		return JWT.create()
			.withSubject(account.getId().toString())
			.withClaim("email", account.getEmail())
			.withClaim("subject", account.getEmail())
			.withClaim("username", account.getEmail())
			.withExpiresAt(new Date(System.currentTimeMillis() + config.getExpiration()))
			.sign(Algorithm.HMAC256(config.getSecret()));
	}
	public DecodedJWT decodeChangePasswordToken(String token) {
		validateToken(token);

		try {
			return JWT.decode(token);
		} catch (JWTDecodeException exception){
			throw new UserPropagableException("Not a valid token. Cannot decode token!");
		}
	}

	public String getUsername(String token) {
		final var claims = decodeChangePasswordToken(token).getClaims();
		return claims.get("subject").asString();
	}

	public boolean validateToken(String token) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(config.getSecret());
			JWTVerifier verifier = JWT.require(algorithm)
				.build();
			DecodedJWT jwt = verifier.verify(token);
			boolean expires = checkIfExpires(jwt);
			return !expires;
		} catch (JWTVerificationException exception){
			return false;
		}
	}

	static boolean checkIfExpires(DecodedJWT jwt) {
		return jwt.getExpiresAt()
			.before(new Date());
	}
}
