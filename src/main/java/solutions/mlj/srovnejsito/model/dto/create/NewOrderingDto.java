package solutions.mlj.srovnejsito.model.dto.create;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class NewOrderingDto {

	@Schema(description = "ID of the Entity", example = "1", required = true)
	@NotNull(message = "Entity ID must be provided.")
	public Integer id;

	@Schema(description = "Int of position in order", example = "1", required = true)
	@NotNull(message = "Entity order must be provided.")
	public Integer sort;

}
