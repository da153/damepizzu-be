package solutions.mlj.srovnejsito.security;

import lombok.experimental.UtilityClass;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import solutions.mlj.srovnejsito.security.model.PasswordHolder;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@UtilityClass
public class SecurityUtils {

    private static final String charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";

    private static BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    public static PasswordHolder generateLoginPassword() {
        String rawPassword = generateRandomPassword(6);
        String encryptedPassword = encryptPassword(rawPassword);
        return new PasswordHolder(rawPassword, encryptedPassword);
    }

    public static String encryptPassword(String rawPassword) {
        return encoder.encode(rawPassword);
    }

    public static String generateRandomPassword(int length) {
        List<Character> charCollection = charset.chars().mapToObj(i -> (char) i).collect(Collectors.toList());
        Collections.shuffle(charCollection);

        StringBuilder stringBuilder = new StringBuilder(length);
        Random random = new Random();
        IntStream.range(0, length).map(i -> random.nextInt(charCollection.size())).mapToObj(charCollection::get).forEach(stringBuilder::append);

        return stringBuilder.toString();
    }

}
